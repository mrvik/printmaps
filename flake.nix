{
  inputs = {
    utils.url = "github:numtide/flake-utils";
  };
  outputs = { self, nixpkgs, utils }: utils.lib.eachDefaultSystem (system:
    let
      pkgs = nixpkgs.legacyPackages.${system};
      name = "buildmap";
      version = "0.0.1";

      dependencies = with pkgs; [ coreutils zip file bash ];

      initScript = pkgs.writeScriptBin "run_printmap" ''
        printmaps_buildservice /config/printmaps_buildservice.yaml &
        printmaps_webservice /config/printmaps_webservice.yaml &
      '';

      official-nik =
        let
          name = "nik4";
          python = pkgs.python3;
          python-dependencies = python.withPackages(pp: [
            pp.python-mapnik
          ]);
        in
        pkgs.python3Packages.buildPythonPackage {
          inherit name;
          src = pkgs.fetchgit {
            url = "https://github.com/Zverik/Nik4.git";
            hash = "sha256-bDJPTkOo8Nl7MVGVyNC0u7/yLwsoYGYxcYNB5GJIp7E=";
            rev = "990f501e716b328119b376333d63c5077860d3e7";
          };

          propagatedBuildInputs = [pkgs.mapnik python python-dependencies];

          installPhase = ''
            mkdir -p "$out/bin"
            cp *.py "$out/bin"
          '';
        };
    in
    {
      packages = rec {
        inherit official-nik;

        builder = pkgs.callPackage ./printmaps_buildservice/. {};
        client = pkgs.callPackage ./printmaps_client/. {};
        webservice = pkgs.callPackage ./printmaps_webservice/. {};
        nik4 = pkgs.callPackage ./Nik4/. {};
        config = pkgs.callPackage ./configFiles.nix {};

        binaries = pkgs.buildEnv {
          name = "${name}-bin";
          paths = [ builder client webservice nik4 ] ++ dependencies;
        };

        container = pkgs.dockerTools.buildLayeredImage {
          inherit name;
          tag = "v${version}";

          contents = dependencies ++ [
            pkgs.dockerTools.usrBinEnv
            pkgs.dockerTools.caCertificates
            binaries
            (pkgs.buildEnv {
              name = "${name}-config";
              paths = [ config ];
              pathsToLink = [ "/config" ];
            })
            (pkgs.buildEnv{
              name = "${name}-assets";
              paths = [ ./assets/. ];
              extraPrefix = "/assets/";
            })
            initScript
          ];

          config = {
            Cmd = ["${pkgs.bashInteractive}/bin/bash"];
            Volumes = {
              "/data" = {};
            };
            WorkingDir = "/data";
          };
        };

        default = container;
      };

      devShell = pkgs.mkShell {
        buildInputs = with pkgs; [
        ];
      };
    }
  );
}
