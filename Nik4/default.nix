{ lib, pkgs }:
let
  name = "nik4";
  python = pkgs.python3;
  python-dependencies = python.withPackages(pp: [
    pp.python-mapnik
  ]);
in
pkgs.python3Packages.buildPythonPackage {
  inherit name;
  src = ./.;

  propagatedBuildInputs = [pkgs.mapnik python python-dependencies];

  installPhase = ''
    mkdir -p "$out/bin"
    cp *.py "$out/bin"
  '';
}
