from setuptools import setup

setup(
  name='nik4',
  #packages=['someprogram'],
  version='0.0.1',
  #author='...',
  #description='...',
  install_requires=[],
  scripts=[
    'nik4-printmaps.py',
    'nik4-org.py',
  ],
  entry_points={
    # example: file some_module.py -> function main
    #'console_scripts': ['someprogram=some_module:main']
  },
)
