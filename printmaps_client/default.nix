{ buildGoModule, lib }:
buildGoModule {
  pname = "printmaps";
  version = "0.0.1";
  src = ./.;

  vendorHash = "sha256-w3DRCXQafWNoZ4lhT1xB0N/nxzgjZD7FKMhyY583clE=";

  CGO_ENABLED = 0;

  meta = with lib; {
    description = "Printmaps client";
    license = licenses.mit;
  };

  postInstall = ''
    mv "$out/bin/printmaps_client" "$out/bin/printmaps"
  '';
}
