module github.com/printmaps/printmaps/printmaps_client

go 1.16

require (
	github.com/StefanSchroeder/Golang-Ellipsoid v0.0.0-20200928074047-3758eb9e9574
	github.com/davecgh/go-spew v1.1.1
	github.com/im7mortal/UTM v1.2.0
	github.com/paulmach/orb v0.2.2
	github.com/yuin/gopher-lua v0.0.0-20210529063254-f4c35e4016d9
	gitlab.com/mrvik/printmaps/pd v0.0.0-20240210131102-597e12ec4c9b
	gopkg.in/yaml.v2 v2.4.0
)
