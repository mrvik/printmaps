{ stdenv, buildEnv, lib }:
let
  paths = [
    "./printmaps_buildservice/printmaps_buildservice.yaml"
    "./printmaps_webservice/printmaps_webservice.yaml"
    "./printmaps_webservice/printmaps_webservice_capabilities.json"
    "./printmaps_webservice/printmaps_webservice_maintenance.html"
    "./printmaps_webservice/printmaps_webservice.yaml"
  ];
  concat = lib.lists.foldr (a: b: a+"\n"+b) "";
in
  stdenv.mkDerivation {
    name = "printmaps-config";
    src = ./.;
    installPhase = with lib.lists; ''
      set -eux
      mkdir -p "$out/config"
      ${concat (forEach paths (path: ''cp -v ${path} "$out/config/"''))}
    '';
  }
