module github.com/printmaps/printmaps/printmaps_buildservice

go 1.16

require (
	gitlab.com/mrvik/printmaps/pd v0.0.0-20240210131102-597e12ec4c9b
	gopkg.in/yaml.v2 v2.4.0
)
