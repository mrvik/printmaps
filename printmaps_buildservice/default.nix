{ buildEnv, buildGoModule, lib }@pkgs:
buildGoModule {
  pname = "printmaps_buildservice";
  version = "0.0.1";
  src = ./.;

  vendorHash = "sha256-RyuHaaAGtIhV8rhINQU55atZOoJnxXwbOt0Lx80tuaM=";

  CGO_ENABLED = 0;

  meta = with lib; {
    description = "Map build service";
    license = licenses.mit;
  };
}
