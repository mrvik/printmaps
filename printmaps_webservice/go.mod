module github.com/printmaps/printmaps/printmaps_webservice

go 1.16

require (
	github.com/JamesMilnerUK/pip-go v0.0.0-20180711171552-99c4cbbc7deb
	github.com/gofrs/uuid v4.2.0+incompatible
	github.com/julienschmidt/httprouter v1.3.0
	github.com/rs/cors v1.8.2
	gitlab.com/mrvik/printmaps/pd v0.0.0-20240210131102-597e12ec4c9b
	gopkg.in/yaml.v2 v2.4.0
)
