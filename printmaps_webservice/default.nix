{ buildGoModule, lib, pkgs }:
buildGoModule {
  pname = "printmaps_webservice";
  version = "0.0.1";
  src = ./.;

  vendorHash = "sha256-gxwc87VUu1KlOKH+lYU3Dwjzf1tfJDt89mt32V3k0DU=";

  CGO_ENABLED = 0;

  meta = with lib; {
    description = "Printmaps webserver";
    license = licenses.mit;
  };
}
